-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2022 at 04:19 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kerjapraktek`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `image`, `email`, `telepon`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'sistem kerja praktek', '1641704284-PgBt6uqq0Sy5uoN48QCoOlkKRJcQuPBu4fe1j1Op9cF69HRdy0.png', 'demo@mail.com', '5675675675', 'jln testing', NULL, '2022-01-09 04:32:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `frontend`
--

CREATE TABLE `frontend` (
  `id` int(11) NOT NULL,
  `header` longtext DEFAULT NULL,
  `body` longtext DEFAULT NULL,
  `footer` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frontend`
--

INSERT INTO `frontend` (`id`, `header`, `body`, `footer`, `created_at`, `updated_at`) VALUES
(1, '<h1 class=\"text-white\" data-aos=\"fade-up\">Mari bergabung untuk magang bersama kami!\n                                   </h1>', '<h1 class=\"mb-4\" data-aos=\"fade-up\"><span style=\"color: #1fc9fa;\">Badan Pusat</span> <span style=\"color: #2dc26b;\">Statistik</span> <span style=\"color: #f1c40f;\">Kota Denpasar</span></h1>\r\n<p class=\"mb-0\" style=\"line-height: 1.1;\" data-aos=\"fade-up\"><span style=\"color: #333333; font-family: \'trebuchet ms\', geneva, sans-serif; font-size: 18pt; background-color: #ffffff;\">BPS merupakan institusi penyelenggara yang dibentuk berdasarkan undang-undang untuk melaksanakan kegiatan pelayanan publik. Berdasarkan Undang-Undang Nomor 25 Tahun 2009 tentang Pelayanan Publik yang pelaksanaannya diatur Peraturan Pemerintah No. 96 Tahun 2012, pada Pasal 11 disampaikan bahwa penyelenggara pelayanan publik dapat menyelenggarakan sistem pelayanan terpadu yang bertujuan untuk:</span></p>', '<h1 class=\"text-white\" data-aos=\"fade-up\" data-aos-delay=\"100\"><span style=\"color: #ee8500;\">Informasi lebih lanjut</span> <span style=\"font-size: 18pt;\">bisa menghubungi kontak yang tertera.</span></h1>', NULL, '2022-01-09 05:45:25');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id` bigint(20) NOT NULL,
  `rincian` longtext DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id`, `rincian`, `updated_at`) VALUES
(1, '<h2><span style=\"font-size: 36pt;\"><span style=\"color: #843fa1;\">JUDUL </span><span style=\"color: #169179;\">NI </span><span style=\"color: #2dc26b;\">BRO</span></span></h2>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: left;\">INGET DIBACA Sidang Kerja PraktikTahun Ajaran 2019-2020 Program Studi Teknik MekatronikaDepartemen Teknik Mekanika dan 13. Balai Riset dan Standardisasi Industri di Banda Aceh, Medan, Padang, Palembang, Bandar Lampung, Surabaya, Banjarbaru, Pontianak, Samarinda, Manado, dan Ambon. 14. Balai Sertifikasi Industri di Jakarta. 15. Balai Pengembangan Produk dan Standarisasi Industri Pekanbaru. 16. Balai Pengembangan Industri Persepatuan Indonesia di Sidoarjo.</p>\r\n<p>1.gergerg</p>\r\n<p>2.grwwrgwrgw</p>\r\n<p>3.rgrgrgrg</p>', '2022-01-05 19:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_05_21_020333_create_tb_store__table', 1),
(5, '2021_05_22_033242_create_role_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `no` varchar(255) DEFAULT NULL,
  `institusi` varchar(255) DEFAULT NULL,
  `program_studi` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `no_telepon` varchar(255) DEFAULT NULL,
  `pembina_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bagian` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `file_peserta` varchar(255) DEFAULT NULL,
  `file_admin` varchar(255) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `no_surat` varchar(255) DEFAULT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id`, `name`, `no`, `institusi`, `program_studi`, `jenis_kelamin`, `no_telepon`, `pembina_id`, `user_id`, `bagian`, `status`, `start`, `end`, `file_peserta`, `file_admin`, `kepada`, `no_surat`, `tanggal_surat`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'bino maskumambang', '235252342342', 'STIKOM BALI', 'Sistem Informasi', 'Laki-laki', '5464564', 21, 19, 'programmer', 5, NULL, NULL, NULL, NULL, 'Dekan Fakultas Informatika & Komputer', '069.A/DIRAKAKEM/WRI/ITBSTIKOM/II/21', '2022-01-16', NULL, '2022-01-02 23:02:20', NULL),
(2, 'peserta', '32434234252345', 'STIKOM', 'Sistem Informasi', 'Perempuan', '2342135421', 21, 20, 'IT', 5, '2022-01-11', '2022-03-09', NULL, NULL, 'Dekan Fakultas Informatika & Komputer UNUD', '069.A/DIRAKAKEM/WRI/ITBSTIKOM/II/21', '2022-01-16', '2022-01-02 20:09:59', '2022-01-29 06:19:05', NULL),
(4, 'beno', '234234523452', 'STIKOM', 'SI', 'Laki-laki', '423452452', NULL, 26, 'abc', 1, '2022-01-16', '2022-02-04', '1642303142.pdf', NULL, 'Dekan Fakultas Informatika & Komputer', '069.A/DIRAKAKEM/WRI/ITBSTIKOM/II/21', '2022-01-16', '2022-01-15 19:19:46', '2022-01-31 19:19:15', '2022-01-31 19:19:15'),
(6, 'sukma sanjani', '234245254234', 'STIKOM', 'SI', 'Perempuan', '234242342', 21, 27, 'abc', 3, '2022-01-01', '2022-01-31', '16423219709-fcHcZmx.pdf', '1642324260-De4rU4wA.pdf', 'Dekan Fakultas Informatika & Komputer', '069.A/DIRAKAKEM/WRI/ITBSTIKOM/II/21', '2022-01-16', '2022-01-16 00:32:50', '2022-01-29 06:18:30', '2022-01-29 06:18:30'),
(7, 'nyoman ambyar', '23452454245', 'undiksha', 'sastra', 'Laki-laki', '234523452', 21, 28, 'abc', 5, '2022-01-14', '2022-03-11', '1642322733-OXgkFjem.pdf', '1642324054-Hd9AQpkc.pdf', 'Dekan Fakultas Informatika & Komputer', '069.A/DIRAKAKEM/WRI/ITBSTIKOM/II/21', '2022-01-16', '2022-01-16 00:45:33', '2022-01-31 19:19:03', '2022-01-31 19:19:03'),
(8, 'mayang', '234123412', 'undiksha', 'sastra', 'Perempuan', '2345234', 21, 29, 'rsagaegh', 3, '2022-01-04', '2022-02-04', '1643023665-mzEgcMMt.pdf', NULL, 'Dekan Fakultas Informatika & Komputer', '234892/MAKAKA/TEST/STIKOM', '2022-01-31', '2022-01-24 03:27:45', '2022-01-31 19:19:08', '2022-01-31 19:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL),
(2, 'Pembimbing', NULL, NULL),
(3, 'Peserta', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pangkat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `peserta_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idi` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `nip`, `pangkat`, `jabatan`, `role_id`, `store_id`, `peserta_id`, `status`, `image`, `email`, `idi`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'surya', '345346532542342', 'Pembina Tk.I', 'Sekretaris', 2, 1, NULL, 1, NULL, 'surya@mail.com', '1341234134123', NULL, '$2y$10$AB7gC/UFZ9.MwoFATBcuZOZ/JjCedeiFPgIf78yx4m8CUWfRvgOM6', NULL, NULL, '2022-01-02 19:46:19', '2021-11-18 20:14:56'),
(7, 'ujang', NULL, NULL, NULL, 3, 2, NULL, NULL, NULL, 'ujang@mail.com', '', NULL, '$2y$10$Bi7IiS5.xJX4kwwj/ci5/unQHjGav1C/ZnHXz45jfP9XBDlksGBAK', NULL, '2021-05-25 02:05:16', '2022-01-21 06:37:00', '2022-01-21 06:37:00'),
(8, 'admin2', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'admin2@admin.com', '', NULL, '$2y$10$KS0rsekzWC1JCDge7l5t4e9ZxIlGR72aUK.IS3bBhTAWW3JDulh/u', NULL, '2021-05-25 02:05:35', '2021-05-25 02:05:35', NULL),
(15, 'admin', NULL, NULL, NULL, 1, NULL, NULL, 1, '1637308659-UC72fx7xjt1WwwTqqDQxtQXzpt7YUkvvxOs3rtSzgTd2PPuBMd.jpg', 'admin@admin.com', NULL, NULL, '$2y$10$9zdim5JlYH2txkBAHZoMMuyBuClq0NAfVBPVSP5HuBwJJXRos4C/m', NULL, '2021-10-15 21:21:10', '2021-11-19 00:57:39', NULL),
(16, 'saktiaa', NULL, NULL, NULL, 3, NULL, NULL, 1, NULL, 'sakti@mail.com', NULL, NULL, '$2y$10$4h3LE7y7xdA4AkLJcHdbn.5ZxRUWjqUiogjKJCG1KtIuQqKayVWr6', NULL, '2021-10-15 22:52:07', '2022-01-21 06:37:14', '2022-01-11 06:37:14'),
(19, 'bino', NULL, NULL, NULL, 3, NULL, 1, 1, NULL, 'binomo@mail.com', NULL, NULL, '$2y$10$Ugcfeu8iwlVbSI19O5NB.OGo/usaBHM60AvEifdwfqW3/TPMUvxGm', NULL, '2021-12-11 00:39:43', '2021-12-11 00:39:43', NULL),
(20, 'peserta', NULL, NULL, NULL, 3, NULL, 2, 1, NULL, 'peserta@demo.com', NULL, NULL, '$2y$10$xpvp1Ld1VkGWsflndBwfxeqYPHwNWPgK7g0GJZw1RphEbFBJXwVd.', NULL, '2022-01-02 02:16:36', '2022-01-02 20:09:59', NULL),
(21, 'pembimbing', '43532452432342', 'Pembina Tk.I', 'Sekretaris A', 2, NULL, NULL, 1, '1641119069-IK1Zh7HQqRWIYZ5uBp6yC9v9qU4EAx5sTqDOs1oCz8vzRFLHoi.jpg', 'pembimbing@demo.com', NULL, NULL, '$2y$10$aDqkb.IVWH.CM1F4uj7NJuMVEyXaOpLL/2JJoZ9C6xHzK5dG0Qa1W', NULL, '2022-01-02 02:19:30', '2022-01-08 20:12:32', NULL),
(22, 'adit', NULL, NULL, NULL, 3, NULL, NULL, 1, '1641455379-88iwV5Io6AbqvoCGdpOv2GaFLsUSx3IfutPEXZT4muZiwN2hn8.jpg', 'adit@demo.com', NULL, NULL, '$2y$10$b47DdYPjb.tgV.fFDqwc..DU3BhEYjxsOt1ofPnbm7B8wqTYsLeBa', NULL, '2022-01-06 00:08:37', '2022-01-06 00:51:22', NULL),
(23, 'Dinda', NULL, NULL, NULL, 3, NULL, 3, 1, NULL, 'Dinda@gmail.com', NULL, NULL, '$2y$10$v61sibGfOwTWYYBTzO37pe7a54bjtpW0RzAiTA/4jG1wc0DEcaVX.', NULL, '2022-01-10 04:09:03', '2022-01-31 19:18:41', '2022-01-31 19:18:41'),
(24, 'testing', NULL, NULL, NULL, 3, NULL, NULL, 1, NULL, 'testing@demo.com', NULL, NULL, '$2y$10$CetGx/qXQjNi0sfEgfYC9OLheO44zTIR1A55teOjoZjc57dvPrFGa', NULL, '2022-01-10 04:31:38', '2022-01-31 19:19:26', '2022-01-31 19:19:26'),
(26, 'beno', NULL, NULL, NULL, 3, NULL, 4, 1, NULL, 'beno@mail.com', NULL, NULL, '$2y$10$q5CqEYVjiZ2w8ILGsJS1suv3MtUZLwG8NvXPlb8LS8ojd5AAz/7fa', NULL, '2022-01-15 06:57:32', '2022-01-31 19:19:15', '2022-01-31 19:19:15'),
(27, 'sukma', NULL, NULL, NULL, 3, NULL, 6, 1, NULL, 'sukma@mail.com', NULL, NULL, '$2y$10$qXG76Bdxa5boK1nC31XcvOoIQBOLdRLxCDp3pvC0PQbi/fdP23wyq', NULL, '2022-01-16 00:26:57', '2022-01-29 06:18:30', '2022-01-29 06:18:30'),
(28, 'nyoman', NULL, NULL, NULL, 3, NULL, 7, 1, NULL, 'nyoman@mail.com', NULL, NULL, '$2y$10$g7NugtK/IF8fiRCAbIFa7.ExSLylXcFR9EptsyjgJ8BbKzfsUI9hm', NULL, '2022-01-16 00:44:00', '2022-01-31 19:19:03', '2022-01-31 19:19:03'),
(29, 'mayang', NULL, NULL, NULL, 3, NULL, 8, 1, NULL, 'mayang@demo.com', NULL, NULL, '$2y$10$3/ynpqOEF5X2wwLZxT7PEeDdJawYyp8jee47W2Ynk1H8sPHn/YDCy', NULL, '2022-01-24 03:27:03', '2022-01-31 19:19:08', '2022-01-31 19:19:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend`
--
ALTER TABLE `frontend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frontend`
--
ALTER TABLE `frontend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
