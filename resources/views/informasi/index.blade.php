@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Informasi Rincian</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
               @if( Auth::user()->role_id == 1)
               <div class="card-header">
                <div style="float: right; padding: 5px">
                <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                    <li class="">
                        <button type="button" class="btn btn-outline-primary" onclick="window.location.href='{{ route('informasi.edit') }}'" style="background-color: #56d4f3; border:none;">Edit</button>
                        {{-- <button type="submit" class="btn btn-outline-danger" onclick="deleteAllConfirm()" form="deleteall">Delete Selected</button> --}}
                    </li>
                      {{-- <div class="ml-auto d-inline-flex">
                          <div class="dropdown">
                              <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Export
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                              </ul>
                          </div>
                      </div> --}}
                    </ul>
              </div>
              <br>
              </div>
              @endif
              <div class="card-body">
              <div id="a">
                {!! $item->rincian !!}
              </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection

@section('js')
 <script>
    $('#a').tinyMCE().getContent();
});</script>   
@endsection