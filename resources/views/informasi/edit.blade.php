@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('informasi') }}">Informasi</a></li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('informasi.update') }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>Input Informasi Rincian</h5>
                <br>
                <textarea id="mytextarea" class="textarea h-24 textarea-bordered textarea-success form-control" placeholder="Informasi Rincian" style="height: 300px" name="rincian">{{$item->rincian}}</textarea>
                  </div>
                  <div class="col-sm-10">
                    <div class="text-right">
                      <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('informasi') }}'">cancel</button>
                      <button type="submit" class="btn btn-save" style="">Save</button>
                    </div>
                  </div>
                  <br><br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection