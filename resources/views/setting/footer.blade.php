@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Halaman Depan</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('setting.footer-update') }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>Input Informasi Rincian</h5>
                <br>
                <hr>
                <div class="tabs">
                    <a href="{{ route('setting.header') }}" class="tab">Header</a> 
                    <a href="{{ route('setting.body') }}" class="tab tab-active">Body</a> 
                    <a href="{{ route('setting.footer') }}" class="tab {{ Request::is('setting/footer') ? 'tab-active' :'' }}">Footer</a>
                </div>
                <hr>
                <br>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Footer</label>
                    <div class="col-sm-10">
                        <textarea id="mytextarea" class="textarea h-24 textarea-bordered textarea-success form-control" placeholder="footer" style="height: 300px" name="footer">{{$item->footer}}</textarea>
                @error('footer')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  </div>
                  <div class="col-sm-10">
                    <div class="text-right">
                      <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('dashboard') }}'">cancel</button>
                      <button type="submit" class="btn btn-save" style="">Save</button>
                    </div>
                  </div>
                  <br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection