@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Basic Setting</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('setting.basic-update') }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>Edit Profile</h5>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Image</label>
                  <div class="col-sm-10">
                <input type="file" class="dropify" name="image" @if($item->image != null)  data-default-file="{{ asset('images/'.$item->image) }}" @endif> <!-- plugin input image-->
                {{-- @if(Session::has('message'))
                  <p style="border: none; background-color: white; color: red;" class="alert {{ Session::get('alert-class', 'alert- info') }}">
                  {{ Session::get('message') }}
                  </p>
                @endif --}}
                    </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
              <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->name }}"> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
               <!--Email-->
               <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">email</label>
                  <div class="col-sm-10">
              <input type="text" name="email" class="form-control input input-bordered @error('email') is-invalid @enderror" id="address" placeholder="" value="{{ $item->email }}">
              @error('email')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror 
                  </div>
                </div>
            <div class="form-group row" id="telepon">
              <label for="staticEmail" class="col-sm-2 col-form-label">telepon</label>
              <div class="col-sm-10">
          <input type="number" name="telepon" class="form-control input input-bordered @error('telepon') is-invalid @enderror" id="telepon" placeholder="" value="{{ $item->telepon }}"> 
          @error('telepon')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>
            <div class="form-group row" id="alamat">
              <label for="staticEmail" class="col-sm-2 col-form-label">alamat</label>
              <div class="col-sm-10">
          <input type="text" name="alamat" class="form-control input input-bordered @error('alamat') is-invalid @enderror" id="alamat" placeholder="" value="{{ $item->alamat }}"> 
          @error('alamat')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>  
           <input type="hidden" name="id" value="{{ $item->id }}">
            <div class="form-group row">
              <label for="password-confirm" class="col-sm-2 col-form-label"></label>

              <div class="col-sm-10">
                <div class="text-right">
                  <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('dashboard') }}'">cancel</button>
                  <button type="submit" class="btn btn-save" style="">Save</button>
                </div>
              </div>
          </div>    
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection

@section('js')
<script>
    $("#input-pass").hide();
    $("#input-confirm").hide();
    $("#no-cp").hide();
  $('#cp').on('change keyup', function() {
    var get_cp = $("#cp").val();
    console.log(get_cp)
    if (get_cp == "yes") {
        $("#input-pass").show();
        $("#input-confirm").show();
        $("#no-cp").show();
        console.log("no nih")
    }else{
        $("#input-pass").hide();
        $("#input-confirm").hide();
        $("#no-cp").hide();
    }
  });
</script>
@endsection