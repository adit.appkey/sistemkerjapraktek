<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
    <li class="nav-item has-treeview">
      <a href="{{ route('dashboard') }}" @if(Route::is('dashboard')) class="nav-link active" @else class="nav-link"  @endif>
        <i class="nav-icon fas fa-chart-pie"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('user') }}"@if(Route::is('user')|| Route::is('user.create') || Route::is('user.edit'))) class="nav-link active" @else class="nav-link"  @endif>
        <i class="nav-icon fas fa-users"></i>
        <p>
          User
        </p>
      </a>
    </li>
    <li class="{{Request::is('peserta*') ? 'menu-open' :'' }} nav-item has-treeview">
      <a href="#" class="nav-link {{Request::is('peserta*') ? 'active' :'' }} ">
        <i class="nav-icon fas fa-user"></i>
        <p>
          Peserta Kerja Praktek
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('peserta.daftar') }}"class="{{ Request::is('peserta/daftar') ? 'active' :'' }} nav-link ">
            <i class="nav-icon fas fa-id-card"></i>
            <p>
              Daftar Permohonan
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('peserta.pending') }}" class="{{ Request::is('peserta/pending') ? 'active' :'' }} nav-link">
            <i class="fas fa-user-clock nav-icon"></i>
            <p>Menunggu Verifikasi</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('peserta.verified') }}" class="{{ Request::is('peserta/verified') ? 'active' :'' }} nav-link">
            <i class="fas fa-user-check nav-icon"></i>
            <p>Terdaftar</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('peserta.rejected') }}" class="{{ Request::is('peserta/rejected') ? 'active' :'' }} nav-link">
            <i class="fas fa-user-alt-slash nav-icon"></i>
            <p>Tertolak</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="{{ route('alumni') }}"@if(Route::is('alumni')) class="nav-link active" @else class="nav-link"  @endif>
        <i class="fas fa-user-graduate nav-icon"></i>
        <p>
          Alumni Kerja Praktek
        </p>
      </a>
    </li>
    <li class="{{Request::is('informasi*') ? 'menu-open' :'' }} nav-item has-treeview">
      <a href="#" class="nav-link {{Request::is('informasi*') ? 'active' :'' }} ">
        <i class="far fa-newspaper nav-icon"></i>
        <p>
          Informasi
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('informasi') }}"class="{{ Request::is('informasi*') ? 'active' :'' }} nav-link ">
            <i class="far fa-newspaper nav-icon"></i>
            <p>
              Informasi Rincian
            </p>
          </a>
        </li>
      </ul>
    </li>
    <li class="{{Request::is('setting*') ? 'menu-open' :'' }} nav-item has-treeview">
      <a href="#" class="nav-link {{Request::is('setting*') ? 'active' :'' }} ">
        <i class="fas fa-sliders-h nav-icon"></i>
        <p>
          setting
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('setting.basic') }}"class="{{ Request::is('setting/basic') ? 'active' :'' }} nav-link ">
            <i class="fas fa-circle nav-icon"></i>
            <p>
              Aplikasi
            </p>
          </a>
        </li>
      </ul>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('setting.header') }}"class="{{ Request::is('setting/header') || Request::is('setting/body') || Request::is('setting/footer') ? 'active' :'' }} nav-link ">
            <i class="fas fa-circle nav-icon"></i>
            <p>
              Halaman Depan
            </p>
          </a>
        </li>
      </ul>
    </li>
  </ul>