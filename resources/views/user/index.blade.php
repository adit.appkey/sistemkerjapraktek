@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Store List</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Store List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <div style="float: right; padding: 5px">
                  <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                      <li class="">
                          <button type="button" class="btn btn-outline-primary" onclick="window.location.href='{{ route('user.create') }}'" style="background-color: #56d4f3; border:none;">Create</button>
                          {{-- <button type="submit" class="btn btn-outline-danger" onclick="deleteAllConfirm()" form="deleteall">Delete Selected</button> --}}
                      </li>
                        {{-- <div class="ml-auto d-inline-flex">
                            <div class="dropdown">
                                <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                  Export
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                  <li><a class="dropdown-item" href="#">Action</a></li>
                                  <li><a class="dropdown-item" href="#">Another action</a></li>
                                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </div>
                        </div> --}}
                      </ul>
                </div>
                <br>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <!-- SEARCH FORM -->
                <div class="row">
                  <div class="form-group col-2">
                      <select id="id_role" name="id_role" class="select select-bordered select-info w-full max-w-xs" placeholder="Order Status">
                      <option value=""> <center>all role</center> </option>
                      @foreach ($roles as $role)
                          <option value="{{ $role->id }}">{{ $role->name }}</option>
                      @endforeach
                      </select>
                  </div>
                </div>
                <div class="table-responsive">
                  <table id="example" class="table w-full  table-zebra">
                    <thead>
                    <tr>
                      {{-- <th style="min-width:30px">
                        <input type="checkbox" id="selectAll" class="main" style="width: 50px;">
                      </th> --}}
                      <th>NO</th>
                      <th>Name</th>
                      <th>Role</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    {{-- <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach($user as $item)
                    <tr>
                        <td><input type="checkbox" class="checkthis" style="width: 50px;" value="{{ $item->id }}" name="ids[]"></td>
                        <td>{{ $no++}}</td>
                        <td>{{ $item->name}}</td>
                        <td>{{ $item->store_id}}</td>
                        <td>{{ $item->role->name}}</td>
                        <td>
                            <center>
                                @if ($item->status == 1)
                                <div style="width: 60px; height: 25px; background-color: #28A745; color: white; text-align: center; border-radius: 40px">
                                    active
                                </div> 
                                @else 
                                <div style="width: 60px; height: 25px; background-color: #F65767; color: white; text-align: center; border-radius: 40px">
                                    deactive
                                </div> 
                                @endif
                            </center>
                        </td>
                        <td>
                          <center> 
                            <a href="{{ route('user.edit',['id'=>$item->id]) }}"><img src="{{ asset('/icon/edit.png') }}" style="width:40px;"></a>
                            <a href="#" onclick="deleteConfirm('{{ $item->id }}')"><img src="{{ asset('/icon/delete.png') }}" style="width:40px;"></a>
                          </center>
                      </td>
                    </tr>
                    @endforeach
                    </tbody> --}}
                  </table>
                </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
@endsection
@section('js')
<script>
  function deleteConfirm(id){
    var url = "{{ url('user/delete') }}/"+id
    Swal.fire({
      title: "are you sure?",
      text: "to delete this data?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#FFAC40',
      cancelButtonColor: '#d33',
      cancelButtonText: "@lang('val.no')",
      confirmButtonText: "@lang('val.yes')"
    }).then((result) => {
    if (result.isConfirmed) {
      window.location.href= url;
      // Swal.fire(
      //   'Deleted!',
      //   id,
      //   'success'
      // )
    }
    })
  }

  function deleteAllConfirm(){
    var url = "{{ url('user/deleteall') }}";
    event.preventDefault(); // prevent form submit
    var form = document.forms["deleteAll"]; // storing the form; // storing the form
    Swal.fire({
      title: "are you sure?",
      text: "to delete this data?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#FFAC40',
      cancelButtonColor: '#d33',
      cancelButtonText: "tidak",
      confirmButtonText: "ya"
    }).then((result) => {
    if (result.isConfirmed) {
      // window.location.href= url;
      form.submit(); 
      // Swal.fire(
      //   'Deleted!',
      //   id,
      //   'success'
      // )
    }
    })
  }

  $(document).ready(function () { 
    var oTable = $('#datatable').dataTable({
        stateSave: true
    });

    var allPages = oTable.fnGetNodes();

    $('body').on('click', '#selectAll', function () {
        if ($(this).hasClass('allChecked')) {
            $('input[type="checkbox"]', allPages).prop('checked', false);
        } else {
            $('input[type="checkbox"]', allPages).prop('checked', true);
        }
        $(this).toggleClass('allChecked');
    })
    
});
</script>
<script>
    let table
      let url = "{{ url('user/datatable') }}"
      dataTable(url)
    
      function dataTable(url) {
      table = $('#example').DataTable({
        // "order": [[ 0, "desc" ]],
              processing: true,
              serverSide: true,
              ajax: {
                url: url,
                data: function (data) {
                  data.id_role = jQuery('#id_role').val()
                  data.id_store = jQuery('#id_store').val()
                }
              },
              columns: [
                // { data: 'checkbox', name: 'checkbox', searchable: false },
                { "data": null,"sortable": false, 
           render: function (data, type, row, meta) {
                     return meta.row + meta.settings._iDisplayStart + 1;
                    }  
        },
                // { data: 'image' , name: 'image' },
                { data: 'name' , name: 'name', searchable: true },
                { data: 'role', name: 'role', searchable: true },
                { data: 'status', name: 'description', searchable: true },
                { data: 'action', name: 'action', "className": "text-center", orderable: false, searchable: true }
              ],
              'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        },
                        'render': function(data, type, row) {
                            return'<div class="checkbox"><input type="checkbox" name="ids[]" class="dt-checkboxes" value="'+data+'"><label></label></div>';
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
          });
      };    
      jQuery('#id_role').change(function(){
            table.draw();
      });
      </script>
      <script>
        $('#id_role').on('change keyup', function() {
        var get_role = $("#id_role").val();
        if (get_role == 2) {
            console.log(get_role);
            $("#select-store").removeClass("d-none");
        }else{
          console.log(get_role);
            $("#select-store").addClass("d-none");
        }
      });
    </script>
@endsection