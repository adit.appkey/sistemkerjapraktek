@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('user') }}">User</a></li>
          <li class="breadcrumb-item active" aria-current="page">Create User</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('user.update',['id'=>$user->id]) }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>edit User Form</h5>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Image</label>
                  <div class="col-sm-10">
                <input type="file" class="dropify" name="image" @if($items->image != null)  data-default-file="{{ asset('images/'.$items->image) }}" @endif> <!-- plugin input image-->
                {{-- @if(Session::has('message'))
                  <p style="border: none; background-color: white; color: red;" class="alert {{ Session::get('alert-class', 'alert- info') }}">
                  {{ Session::get('message') }}
                  </p>
                @endif --}}
                    </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
              <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $items->name }}"> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
               <!--Email-->
               <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">email</label>
                  <div class="col-sm-10">
              <input type="text" name="email" class="form-control input input-bordered @error('email') is-invalid @enderror" id="address" placeholder="" value="{{ $items->email }}">
              @error('email')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror 
                  </div>
                </div>
                <div class="form-group row">
                    <label for="cp" class="col-sm-2 col-form-label">Change Password?</label>
                      <div class="col-sm-10">
                      <select class="select select-bordered select-info w-full max-w-xs" name="cp" id="cp">
                        <option value="" id="no-cp">-- Select --</option>
                        <option value="yes" id="no-cp">YES</option>
                        <option value="no" id="no-cp">NO</option>
                    </select>
                  </div>
                </div> 
             <!--password-->
            <div class="form-group row"  id="input-pass">
                <label for="password" class="col-sm-2 col-form-label">password</label>
                <div class="col-sm-10">
                    <input id="password" type="password" class="form-control input input-bordered @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!--confirm password-->
            <div class="form-group row"  id="input-confirm">
                <label for="password-confirm" class="col-sm-2 col-form-label">Confirm Password</label>

                <div class="col-sm-10">
                    <input id="password-confirm" type="password" class="form-control input input-bordered @error('password_confirmation') is-invalid @enderror" name="password_confirmation" autocomplete="new-password">
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!--role-->   
            <div class="form-group row">
                <label for="role" class="col-sm-2 col-form-label">Role</label>
                  <div class="col-sm-10">
                  <select name="role" id="role" class="select select-bordered select-info w-full max-w-xs @error('role') is-invalid @enderror">
                    <option value="{{ $items->role }}" id="no-role" selected disabled>{{ $items->role->name }}</option>
                  {{-- @foreach ($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach --}}
                </select>
                {{-- <label for="role" class="form-control">{{ $items->role->name }}</label> --}}
                @error('role')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
              </div>
            </div> 
            @if($items->role_id == 2)
            <div class="form-group row" id="nip">
              <label for="staticEmail" class="col-sm-2 col-form-label">NIP</label>
              <div class="col-sm-10">
          <input type="number" name="nip" class="form-control input input-bordered @error('nip') is-invalid @enderror" id="nip" placeholder="" value="{{ $items->nip }}"> 
          @error('nip')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>
            <div class="form-group row" id="pangkat">
              <label for="staticEmail" class="col-sm-2 col-form-label">Pangkat</label>
              <div class="col-sm-10">
          <input type="text" name="pangkat" class="form-control input input-bordered @error('pangkat') is-invalid @enderror" id="pangkat" placeholder="" value="{{ $items->pangkat }}"> 
          @error('pangkat')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>
            <div class="form-group row" id="jabatan">
              <label for="staticEmail" class="col-sm-2 col-form-label">Jabatan</label>
              <div class="col-sm-10">
          <input type="text" name="jabatan" class="form-control input input-bordered @error('jabatan') is-invalid @enderror" id="jabatan" placeholder="" value="{{ $items->jabatan }}"> 
          @error('jabatan')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>  
           @endif
            <div class="form-group row">
                <label for="cp" class="col-sm-2 col-form-label">Status</label>
                  <div class="col-sm-10">
                  <select class="select select-bordered select-info w-full max-w-xs" name="status" id="status">
                    <option value="1" id="no-cp" @if($items->status == 1) selected @endif>Active</option>
                    <option value="2" id="no-cp" @if($items->status != 1) selected @endif>Non Active</option>
                </select>
              </div>
            </div> 
            {{-- <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label">desc</label>
              <div class="col-sm-10">
          <textarea name="desc" id="image-tools" cols="30" rows="10"></textarea>
          @error('desc')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div> --}}
            <div class="form-group row">
              <label for="password-confirm" class="col-sm-2 col-form-label"></label>

              <div class="col-sm-10">
                <div class="text-right">
                  <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('user') }}'">cancel</button>
                  <button type="submit" class="btn btn-save" style="">Save</button>
                </div>
              </div>
          </div>    
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection

@section('js')
<script>
    $("#input-pass").hide();
    $("#input-confirm").hide();
    $("#no-cp").hide();
  $('#cp').on('change keyup', function() {
    var get_cp = $("#cp").val();
    console.log(get_cp)
    if (get_cp == "yes") {
        $("#input-pass").show();
        $("#input-confirm").show();
        $("#no-cp").show();
        console.log("no nih")
    }else{
        $("#input-pass").hide();
        $("#input-confirm").hide();
        $("#no-cp").hide();
    }
  });
</script>
@endsection