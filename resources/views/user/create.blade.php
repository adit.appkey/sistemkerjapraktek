@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('user') }}">User</a></li>
          <li class="breadcrumb-item active" aria-current="page">Create User</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('user.store') }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>Create User Form</h5>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                <input type="file" class="dropify input input-bordered" name="image"> <!-- plugin input image-->
                @if(Session::has('message'))
                  <p style="border: none; background-color: white; color: red;" class="alert {{ Session::get('alert-class', 'alert- info') }}">
                  {{ Session::get('message') }}
                  </p>
                @endif
                    </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
              <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ old('name') }}"> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
               <!--Email-->
               <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">email</label>
                  <div class="col-sm-10">
              <input type="text" name="email" class="form-control input input-bordered @error('email') is-invalid @enderror" id="address" placeholder="" value="{{ old('email') }}">
              @error('email')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror 
                  </div>
                </div>
             <!--password-->
            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">password</label>
                <div class="col-sm-10">
                    <input id="password" type="password" class="form-control input input-bordered @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!--confirm password-->
            <div class="form-group row">
                <label for="password-confirm" class="col-sm-2 col-form-label">Confirm Password</label>

                <div class="col-sm-10">
                    <input id="password-confirm" type="password" class="form-control input input-bordered @error('password_confirmation') is-invalid @enderror" name="password_confirmation" autocomplete="new-password">
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!--role-->   
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Role</label>
                  <div class="col-sm-10">
                  <select name="role" id="role" class="select select-bordered select-info w-full max-w-xs @error('role') is-invalid @enderror">
                    <option value="" id="no-role">-- Select Role --</option>
                  @foreach ($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
                @error('role')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
              </div>
            </div> 
            <!--store-->   
            <div class="form-group row" id="nip" class="d-none">
              <label for="staticEmail" class="col-sm-2 col-form-label">NIP</label>
              <div class="col-sm-10">
          <input type="number" name="nip" class="form-control input input-bordered @error('nip') is-invalid @enderror" id="nip" placeholder="" value="{{ old('nip') }}"> 
          @error('nip')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>
            <div class="form-group row" id="pangkat" class="d-none">
              <label for="staticEmail" class="col-sm-2 col-form-label">Pangkat</label>
              <div class="col-sm-10">
          <input type="text" name="pangkat" class="form-control input input-bordered @error('pangkat') is-invalid @enderror" id="pangkat" placeholder="" value="{{ old('pangkat') }}"> 
          @error('pangkat')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>
            <div class="form-group row" id="jabatan" class="d-none">
              <label for="staticEmail" class="col-sm-2 col-form-label">Jabatan</label>
              <div class="col-sm-10">
          <input type="text" name="jabatan" class="form-control input input-bordered @error('jabatan') is-invalid @enderror" id="jabatan" placeholder="" value="{{ old('jabatan') }}"> 
          @error('jabatan')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
          @enderror
              </div>
            </div>
            <div class="form-group row">
              <label for="password-confirm" class="col-sm-2 col-form-label"></label>

              <div class="col-sm-10">
                <div class="text-right">
                  <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('user') }}'">cancel</button>
                  <button type="submit" class="btn btn-save" style="">Save</button>
                </div>
              </div>
          </div>    
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection

@section('js')
<script>
        $("#no-role").hide();
        $("#nip").addClass("d-none");
        $("#jabatan").addClass("d-none");
        $("#pangkat").addClass("d-none");
    $('#role').on('change keyup', function() {
    var get_role = $("#role").val();
    if (get_role == 2) {
        $("#nip").removeClass("d-none");;
        $("#jabatan").removeClass("d-none");;
        $("#pangkat").removeClass("d-none");;
    }else{
      $("#nip").addClass("d-none");
        $("#jabatan").addClass("d-none");
        $("#pangkat").addClass("d-none");
    }
  });
</script>
@endsection