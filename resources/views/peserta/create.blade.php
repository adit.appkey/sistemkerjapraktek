@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Daftar Kerja Praktek</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                @if(Auth::user()->peserta_id != null)
                    <div>
                        Anda sudah mendaftar untuk menjadi peserta kerja praktek!
                        <br>
                        Mohon check status penerimaan!
                        <br>
                        Pastikan status anda telah diterima kerja praktek sebelum download surat balasan !
                    </div>
                    <br>
                    <div class="col">
                      {{-- <div>
                        <button style="float: left;" type="button" class="btn btn-outline-primary" onclick="window.location.href='{{ route('print.pendaftaran') }}'" style="background-color: #56d4f3; border:none;">Download file balasan</button>
                      </div> --}}
                        {{-- <div>
                          <button style="float: right" type="button" class="btn btn-outline-primary" onclick="window.location.href='{{ route('print.downloadfileadmin',['id' => Auth::user()->id]) }}'" style="background-color: #56d4f3; border:none;">Download file balasan</button>
                        </div> --}}
                    </div>
                @else
                <form action="{{ route('peserta.store') }}"method="POST" enctype="multipart/form-data">
                    @csrf
                  <h5>Create Store Form</h5>
                  <br>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ old('name') }}"> 
                @error('name')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">No Induk</label>
                      <div class="col-sm-10">
                  <input type="number" name="no" class="form-control @error('no') is-invalid @enderror" id="no" placeholder="" value="{{ old('no') }}"> 
                  @error('no')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                  @enderror
                      </div>
                    </div>
                  <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">Institusi</label>
                      <div class="col-sm-10">
                  <input type="text" name="institusi" class="form-control @error('institusi') is-invalid @enderror" id="institusi" placeholder="" value="{{ old('institusi') }}"> 
                  @error('institusi')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                  @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">program_studi</label>
                      <div class="col-sm-10">
                  <input type="text" name="program_studi" class="form-control @error('program_studi') is-invalid @enderror" id="program_studi" placeholder="" value="{{ old('program_studi') }}"> 
                  @error('program_studi')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                  @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">jenis_kelamin</label>
                      <div class="col-sm-10">
                          <select name="jenis_kelamin" class="form-control" id="">
                              <option value="">--Pilih--</option>
                              <option value="Laki-laki">Laki-laki</option>
                              <option value="Perempuan">Perempuan</option>
                          </select>
                  @error('jenis_kelamin')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                  @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">No Telepon</label>
                      <div class="col-sm-10">
                  <input type="number" name="no_telepon" class="form-control @error('no_telepon') is-invalid @enderror" id="no_telepon" placeholder="" value="{{ old('no_telepon') }}"> 
                  @error('no_telepon')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                  @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">bagian</label>
                      <div class="col-sm-10">
                  <input type="text" name="bagian" class="form-control @error('bagian') is-invalid @enderror" id="bagian" placeholder="" value="{{ old('bagian') }}"> 
                  @error('bagian')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                  @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Dari Tanggal</label>
                        <div class="col-sm-10">
                    <input type="date" name="start" class=" @error('start') is-invalid @enderror" id="start" placeholder="" value="{{ old('start') }}"> 
                    @error('start')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                       </span>
                    @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Sampai Tanggal</label>
                        <div class="col-sm-10">
                    <input type="date" name="end" class=" @error('end') is-invalid @enderror" id="end" placeholder="" value="{{ old('end') }}"> 
                    @error('end')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                       </span>
                    @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Surat Pengantar KP</label>
                        <div class="col-sm-10" style="max-width: 250px;">
                    <input type="file" class="dropify input input-bordered" name="file_peserta" required> <!-- plugin input image-->
                    @if(Session::has('message'))
                      <p style="border: none; background-color: white; color: red;" class="alert {{ Session::get('alert-class', 'alert- info') }}">
                      {{ Session::get('message') }}
                      </p>
                    @endif
                        </div>
                    </div>
              <div class="form-group row">
                <label for="password-confirm" class="col-sm-2 col-form-label"></label>
  
                <div class="col-sm-10">
                  <div class="text-right">
                    <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('peserta.daftar') }}'">cancel</button>
                    <button type="submit" class="btn btn-save" style="">Save</button>
                  </div>
                </div>
                @endif
          </div>    
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection