@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('peserta.daftar') }}">Peserta Kerja Praktek</a></li>
          <li class="breadcrumb-item active" aria-current="page">Pilih Pembimbing</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('peserta.terimastore',['id' => $item->id]) }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>Pilih pembina</h5>
                <br>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Nama pembina</label>
                  <div class="col-sm-10">
                    <select required name="pembina_id" id="" class="select mantap select-bordered select-info w-full max-w-xs @error('product_id') is-invalid @enderror">
                      <option value="">--Pilih pembina--</option>  
                      @foreach ($pembina as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('pembina_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
              {{-- <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->name }}">  --}}
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
                <br>
                <h5>Surat Balasan</h5>
                <br>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">No Surat</label>
                  <div class="col-sm-10">
              <input required type="text" name="no_surat" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->no_surat }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Kepada</label>
                  <div class="col-sm-10">
              <input required type="text" name="kepada" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->kepada }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Tanggal Surat</label>
                  <div class="col-sm-10">
              <input required type="date" name="tanggal_surat" class=" @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->tanggal_surat }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
                {{-- <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Surat Balasan KP</label>
                  <div class="col-sm-10" style="max-width: 250px;">
              <input type="file" class="dropify input input-bordered" name="file_admin" required> <!-- plugin input image-->
              @if(Session::has('message'))
                <p style="border: none; background-color: white; color: red;" class="alert {{ Session::get('alert-class', 'alert- info') }}">
                {{ Session::get('message') }}
                </p>
              @endif
                  </div>
              </div> --}}
            <div class="form-group row">
              <label for="password-confirm" class="col-sm-2 col-form-label"></label>

              <div class="col-sm-10">
                <div class="text-right">
                  <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('peserta.daftar') }}'">cancel</button>
                  <button type="submit" class="btn btn-save" style="">Save</button>
                </div>
              </div>
          </div>    
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection