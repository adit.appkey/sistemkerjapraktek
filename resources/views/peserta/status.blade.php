@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Status Penerimaan</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
                <div class="card-body">
                    <div class="flex items-center w-full px-4 py-10 bg-cover card bg-base-200" style="background-image: url(&quot;https://media.istockphoto.com/photos/stylish-workspace-with-laptop-picture-id1163497601?b=1&k=20&m=1163497601&s=170667a&w=0&h=YatZATY0ItePRwG4UEgN8N6LCEfPEvIqOOY0zcjknxU=&quot;);">
                      <button style="float: right;" type="button" class="btn btn-save" onclick="window.location.href='{{ route('print.pendaftaran') }}'" style="background-color: #56d4f3; border:none;">Download surat balasan</button> <br>
                        <div class="card glass lg:card-side text-neutral-content">
                          <div class="max-w-md card-body" style="min-width: 700px">
                            <div class="form-group row">
                              <label for="staticEmail" class="col-sm-2 col-form-label">Status</label>
                              <div class="col-sm-10">
                                  <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $status }}" disabled> 
                              </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->name }}" disabled> 
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">NIS / NIK</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->no }}" disabled> 
                                </div>
                              </div>
                              {{-- <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Institusi</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->institusi }}" disabled> 
                                </div>
                              </div> --}}
                              {{-- <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Jurusan</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->program_studi }}" disabled> 
                                </div>
                              </div> --}}
                              {{-- <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->jenis_kelamin }}" disabled> 
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">No Telepon</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->no_telepon }}" disabled> 
                                </div>
                              </div> --}}
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Pembina</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->pembina->name ?? 'belum ditentukan'}}" disabled> 
                                </div>
                              </div>
                              {{-- <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Bagian</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->bagian }}" disabled> 
                                </div>
                              </div> --}}
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Mulai KP</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->start }}" disabled> 
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Sampai</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control input input-bordered @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->end }}" disabled> 
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>
                </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection