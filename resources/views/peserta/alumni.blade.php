@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Alumni Peserta Kerja Praktek</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Alumni Peserta Kerja Praktek</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <div style="float: right; padding: 5px">
                  <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
                      <li class="">
                          {{-- <button type="button" class="btn btn-outline-primary" onclick="window.location.href='{{ route('user.create') }}'" style="background-color: #56d4f3; border:none;">Create</button> --}}
                          {{-- <button type="submit" class="btn btn-outline-danger" onclick="deleteAllConfirm()" form="deleteall">Delete Selected</button> --}}
                      </li>
                        {{-- <div class="ml-auto d-inline-flex">
                            <div class="dropdown">
                                <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                  Export
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                  <li><a class="dropdown-item" href="#">Action</a></li>
                                  <li><a class="dropdown-item" href="#">Another action</a></li>
                                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </div>
                        </div> --}}
                      </ul>
                </div>
                <br>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <!-- SEARCH FORM -->
                <div class="table-responsive">
                  <table id="example" class="table w-full  table-zebra">
                    <thead>
                    <tr>
                      {{-- <th style="min-width:30px">
                        <input type="checkbox" id="selectAll" class="main" style="width: 50px;">
                      </th> --}}
                      <th>NO</th>
                      <th>Name</th>
                      <th>Institusi</th>
                      <th>Program Studi</th>
                      <th>Jenis Kelamin</th>
                      <th>Pembina</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach($items as $item)
                    <tr>
                        <td>{{ $no++}}</td>
                        <td>{{ $item->name}}</td>
                        <td>{{ $item->institusi}}</td>
                        <td>{{ $item->program_studi}}</td>
                        <td>{{ $item->jenis_kelamin}}</td>
                        <td>{{ $item->pembina->name}}</td>
                        <td>
                          {{-- <center> --}}
                            <button class="btn btn-edit" onclick="window.location.href='{{ route('peserta.edit',['id'=>$item->id]) }}'"><i class="fas fa-eye"></i></button>
                            <button class="btn btn-confirm" onclick="selesai('{{ $item->id }}')"><i class="fas fa-check-square"> Print Surat Selesai </i></button>
                            {{-- </center> --}}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
@endsection
@section('js')
<script>
  function selesai(id){
    var url = "{{ url('print/selesai') }}/"+id
    Swal.fire({
      title: "apakah anda yakin?",
      text: "mencetak surat selesai kp?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#FFAC40',
      cancelButtonColor: '#d33',
      cancelButtonText: "tidak",
      confirmButtonText: "ya"
    }).then((result) => {
    if (result.isConfirmed) {
      window.location.href= url;
    }
    })
  }
</script>
<script>
     $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          {
                extend: 'copy',
                className: "btn-export btn"
            }, {
                extend: 'csv',
                title: 'yuk bisa yyukk',
                filename: 'coba dulu',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
                className: "btn-export btn"
            }, {
                extend: 'excel',
                title: 'yuk bisa yyukk',
                filename: 'coba dulu',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
                className: "btn-export btn"
            }, {
                extend: 'pdf',
                title: 'yuk bisa yyukk',
                filename: 'coba dulu',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
                className: "btn-export btn"
            }, {
                extend: 'print',
                className: "btn-export btn"
            },
        ]
    } );
} );
</script>
@endsection