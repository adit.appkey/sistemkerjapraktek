@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </section>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('peserta.daftar') }}">Peserta Kerja Praktek</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit Peserta Kerja Praktek</li>
        </ol>
      </nav>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
              <div class="card-body">
                <form action="{{ route('peserta.update',['id' => $item->id]) }}"method="POST" enctype="multipart/form-data">
                  @csrf
                <h5>Data Peserta</h5>
                <br>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
              <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->name }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
              @error('name')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-2 col-form-label">No Induk</label>
                  <div class="col-sm-10">
              <input type="number" name="no" class="form-control @error('no') is-invalid @enderror" id="no" placeholder="" value="{{ $item->no }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
              @error('no')
                 <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
              @enderror
                  </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Institusi</label>
                    <div class="col-sm-10">
                <input type="text" name="institusi" class="form-control @error('institusi') is-invalid @enderror" id="institusi" placeholder="" value="{{ $item->institusi }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('institusi')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">program studi</label>
                    <div class="col-sm-10">
                <input type="text" name="program_studi" class="form-control @error('program_studi') is-invalid @enderror" id="program_studi" placeholder="" value="{{ $item->program_studi }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('program_studi')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">jenis kelamin</label>
                    <div class="col-sm-10">
                        <select name="jenis_kelamin" class="form-control" id="" @if(Auth::user()->role_id != 1) Disabled @endif>
                            <option value="Laki-laki" @if($item->jenis_kelamin == 'Laki-laki') Selected @endif>Laki-laki</option>
                            <option value="Perempuan" @if($item->jenis_kelamin == 'Perempuan') Selected @endif >Perempuan</option>
                        </select>
                @error('jenis_kelamin')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">No Telepon</label>
                    <div class="col-sm-10">
                <input type="number" name="no_telepon" class="form-control @error('no_telepon') is-invalid @enderror" id="no_telepon" placeholder="" value="{{ $item->no_telepon }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('no_telepon')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">bagian</label>
                    <div class="col-sm-10">
                <input type="text" name="bagian" class="form-control @error('bagian') is-invalid @enderror" id="bagian" placeholder="" value="{{ $item->bagian }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('bagian')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Dari Tanggal</label>
                    <div class="col-sm-10">
                <input type="date" name="start" class=" @error('start') is-invalid @enderror" id="start" placeholder="" value="{{ $item->start }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('start')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Sampai Tanggal</label>
                    <div class="col-sm-10">
                <input type="date" name="end" class="@error('end') is-invalid @enderror" id="end" placeholder="" value="{{ $item->end }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('end')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  @if($item->status != 1)
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Pembina</label>
                    <div class="col-sm-10">
                        <select name="pembina_id" class="form-control" id="" @if(Auth::user()->role_id != 1) Disabled @endif>
                            @foreach ($pembina as $value)
                                <option value="">--Pilih pembina--</option>
                                <option value="{{$value->id}}" @if($item->pembina_id == $value->id) Selected @endif>{{$value->name}}</option>
                            @endforeach
                        </select>
                @error('pembina_id')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  @endif
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Surat Pengantar</label>
                    <div class="col-sm-10">
                      <button style="" type="button" class="btn btn-outline-primary" onclick="window.location.href='{{ route('print.downloadfilepeserta',['id' => $item->id]) }}'" style="background-color: #56d4f3; border:none;">Download</button>
                @error('end')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  @if(Auth::user()->role_id == 1)
                  <br>
                  <h5>Surat Balasan</h5>
                  <br>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">No Surat</label>
                    <div class="col-sm-10">
                <input required type="text" name="no_surat" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->no_surat }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('name')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Kepada</label>
                    <div class="col-sm-10">
                <input required type="text" name="kepada" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->kepada }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('name')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Tanggal Surat</label>
                    <div class="col-sm-10">
                <input required type="date" name="tanggal_surat" class=" @error('name') is-invalid @enderror" id="name" placeholder="" value="{{ $item->tanggal_surat }}" @if(Auth::user()->role_id != 1) Disabled @endif> 
                @error('name')
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                    </div>
                  </div>
                  @endif
            <div class="form-group row">
              <label for="password-confirm" class="col-sm-2 col-form-label"></label>
              <div class="col-sm-10">
                <div class="text-right">
                  @if(Auth::user()->role_id == 1)
                    <button type="button" class="btn btn-del" onclick="window.location.href='{{ route('peserta.daftar') }}'">cancel</button>
                    <button type="submit" class="btn btn-save" style="">Save</button>
                  @endif
                </div>
              </div>
          </div>    
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
    </form>
  </div>
  
@endsection