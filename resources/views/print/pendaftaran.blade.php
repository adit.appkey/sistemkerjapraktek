<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Tahoma,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Tahoma,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_002{font-family:Tahoma,serif;font-size:10.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Tahoma,serif;font-size:10.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Tahoma,serif;font-size:9.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Tahoma,serif;font-size:9.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:9.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:9.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_011{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_012{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="47c6e13a-7671-11ec-a980-0cc47a792c0a_id_47c6e13a-7671-11ec-a980-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:1008px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="{{ asset('image/background1.jpg') }}"width=612 height=1008></div>
<div style="position:absolute;left:239.62px;top:74.18px" class="cls_003"><span class="cls_003">PEMERINTAH KOTA DENPASAR</span></div>
<div style="position:absolute;left:171.91px;top:88.58px" class="cls_003"><span class="cls_003">DINAS  KOMUNIKASI, INFORMATIKA DAN STATISTIK</span></div>
<div style="position:absolute;left:226.90px;top:102.74px" class="cls_002"><span class="cls_002">Jalan Majapahit No. 1  Denpasar    Telp. 431229</span></div>
<div style="position:absolute;left:168.55px;top:114.50px" class="cls_004"><span class="cls_004">Web site : </span><A HREF="http://www.denpasarkota.go.id/">www.denpasarkota.go.id</A> </span></div>
<div style="position:absolute;left:348.12px;top:114.74px" class="cls_004"><span class="cls_004">E-mail: Dikominfo </span><A HREF="http://www.denpasarkota/">www.denpasarkota</A> go.id</div>
<div style="position:absolute;left:360.86px;top:147.41px" class="cls_006"><span class="cls_006">Denpasar,</span></div>
<div style="position:absolute;left:418.46px;top:147.41px" class="cls_006"><span class="cls_006">{{date('d F Y')}}</span></div>
<div style="position:absolute;left:72.02px;top:179.57px" class="cls_007"><span class="cls_007">Nomor</span></div>
<div style="position:absolute;left:121.97px;top:179.57px" class="cls_007"><span class="cls_007">: 423.4/</span></div>
<div style="position:absolute;left:183.09px;top:179.57px" class="cls_007"><span class="cls_007">/DKIS</span></div>
<div style="position:absolute;left:360.14px;top:179.57px" class="cls_007"><span class="cls_007">Kepada :</span></div>
<div style="position:absolute;left:72.02px;top:193.97px" class="cls_007"><span class="cls_007">Sifat</span></div>
<div style="position:absolute;left:121.73px;top:193.97px" class="cls_007"><span class="cls_007">: Biasa</span></div>
<div style="position:absolute;left:324.12px;top:193.97px" class="cls_007"><span class="cls_007">Yth. {{$item->kepada}}</span></div>
<div style="position:absolute;left:72.02px;top:208.61px" class="cls_007"><span class="cls_007">Lamp</span></div>
<div style="position:absolute;left:121.97px;top:208.61px" class="cls_007"><span class="cls_007">: -</span></div>
<div style="position:absolute;left:324.12px;top:208.61px" class="cls_007"><span class="cls_007">{{$item->institusi}}</span></div>
<div style="position:absolute;left:72.02px;top:223.01px" class="cls_007"><span class="cls_007">Perihal</span></div>
<div style="position:absolute;left:119.94px;top:223.01px" class="cls_007"><span class="cls_007">: Kerja Praktek</span></div>
<div style="position:absolute;left:365.90px;top:223.01px" class="cls_007"><span class="cls_007">di -</span></div>
<div style="position:absolute;left:396.14px;top:237.65px" class="cls_011"><span class="cls_011">Tempat</span></div>
<div style="position:absolute;left:180.31px;top:300.79px" class="cls_006"><span class="cls_006">Sehubungan</span></div>
<div style="position:absolute;left:278.95px;top:300.79px" class="cls_006"><span class="cls_006">dengan</span></div>
<div style="position:absolute;left:353.58px;top:300.79px" class="cls_006"><span class="cls_006">surat</span></div>
<div style="position:absolute;left:416.70px;top:300.79px" class="cls_006"><span class="cls_006">Saudara</span></div>
<div style="position:absolute;left:495.17px;top:300.79px" class="cls_006"><span class="cls_006">No:</span></div>
<div style="position:absolute;left:142.85px;top:316.63px" class="cls_006"><span class="cls_006">{{$item->no_surat}} Tanggal {{date('j F Y',strtotime($item->tanggal_surat))}},</span></div>
<div style="position:absolute;left:142.85px;top:332.47px" class="cls_006"><span class="cls_006">perihal Permohonan Kerja Praktek, maka bersama ini disampaikan bahwa</span></div>
<div style="position:absolute;left:142.85px;top:348.31px" class="cls_006"><span class="cls_006">kami dapat menerima mahasiswa sebagai berikut:</span></div>
<div style="position:absolute;left:180.07px;top:380.26px" class="cls_006"><span class="cls_006">1.  Nama</span></div>
<div style="position:absolute;left:324.12px;top:380.26px" class="cls_006"><span class="cls_006">: {{$item->name}}</span></div>
<div style="position:absolute;left:198.31px;top:396.10px" class="cls_006"><span class="cls_006">NIM</span></div>
<div style="position:absolute;left:324.12px;top:396.10px" class="cls_006"><span class="cls_006">: {{$item->no}}</span></div>
<div style="position:absolute;left:177.19px;top:491.16px" class="cls_006"><span class="cls_006">Untuk melaksanakan Kerja Praktek di Dinas Komunikasi, Informatika</span></div>
<div style="position:absolute;left:142.85px;top:507.24px" class="cls_006"><span class="cls_006">dan Statistik Kota Denpasar dengan syarat harus mengikuti peraturan yang</span></div>
<div style="position:absolute;left:142.85px;top:523.08px" class="cls_006"><span class="cls_006">berlaku dan tetap mengikuti Protokol Kesehatan Pencegahan COVID-19.</span></div>
<div style="position:absolute;left:177.19px;top:538.92px" class="cls_006"><span class="cls_006">Sebagai   Pertanggungjawaban   bahwa   yang   bersangkutan   sudah</span></div>
<div style="position:absolute;left:142.85px;top:554.76px" class="cls_006"><span class="cls_006">menyelesaikan   tugasnya   ditempat   kami,   yang   bersangkutan   agar</span></div>
<div style="position:absolute;left:142.85px;top:570.60px" class="cls_006"><span class="cls_006">menyampaikan  Laporan Akhir Kerja Praktek kepada Dinas Komunikasi,</span></div>
<div style="position:absolute;left:142.85px;top:586.46px" class="cls_006"><span class="cls_006">Informatika dan Statistik Kota Denpasar.</span></div>
<div style="position:absolute;left:171.19px;top:602.30px" class="cls_006"><span class="cls_006">Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan</span></div>
<div style="position:absolute;left:142.85px;top:618.14px" class="cls_006"><span class="cls_006">terima kasih.</span></div>
<div style="position:absolute;left:362.30px;top:730.73px" class="cls_006"><span class="cls_006">A.n </span><span class="cls_007">Kepala Dinas Komunikasi,</span></div>
<div style="position:absolute;left:341.88px;top:744.65px" class="cls_007"><span class="cls_007">Informatika dan Statistik Kota Denpasar,</span></div>
<div style="position:absolute;left:408.62px;top:757.37px" class="cls_007"><span class="cls_007">Sekretaris,</span></div>
<div style="position:absolute;left:355.82px;top:820.51px" class="cls_012"><span class="cls_012">Drs. Dewa Made Ariawan,M.Si</span></div>
<div style="position:absolute;left:362.54px;top:834.19px" class="cls_006"><span class="cls_006">NIP.19640519 199202 1 001</span></div>
</div>

</body>
</html>
