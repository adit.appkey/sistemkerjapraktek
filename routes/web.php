<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/', 'FrontController@index')->name('frontend');
Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'checkroleadmin'], function () {
        Route::group(['prefix' => 'dashboard'], function () {
            route::get('/', 'DashboardController@index')->name('dashboard');

            route::get('profile', 'DashboardController@profile')->name('dashboard.profile');
            route::post('update', 'DashboardController@update')->name('dashboard.update');
        });

        Route::group(['prefix' => 'user'], function () {
            route::get('/', 'UsersController@index')->name('user');
            route::get('create', 'UsersController@create')->name('user.create');
            route::post('store', 'UsersController@store')->name('user.store');
            route::get('edit/{id}', 'UsersController@edit')->name('user.edit');
            route::post('update/{id}', 'UsersController@update')->name('user.update');
            route::get('delete/{id}', 'UsersController@delete')->name('user.delete');
            route::post('deleteall', 'UsersController@deleteall')->name('user.deleteall');
            route::post('upload', 'UsersController@upload')->name('user.upload');
            route::get('datatable', 'UsersController@datatable')->name('user.datatable');
        });
        Route::group(['prefix' => 'peserta'], function () {
            route::get('/daftar', 'PesertaController@daftar')->name('peserta.daftar');
            route::get('/pending', 'PesertaController@pending')->name('peserta.pending');
            route::get('/verified', 'PesertaController@verified')->name('peserta.verified');
            route::get('/rejected', 'PesertaController@rejected')->name('peserta.rejected');
            route::get('create', 'PesertaController@create')->name('peserta.create');
            route::post('store', 'PesertaController@store')->name('peserta.store');
            route::get('edit/{id}', 'PesertaController@edit')->name('peserta.edit');
            route::post('update/{id}', 'PesertaController@update')->name('peserta.update');
            route::get('delete/{id}', 'PesertaController@delete')->name('peserta.delete');
            route::get('terima/{id}', 'PesertaController@terima')->name('peserta.terima');
            route::post('terimastore', 'PesertaController@terimastore')->name('peserta.terimastore');
            route::get('tolak/{id}', 'PesertaController@tolak')->name('peserta.tolak');
            route::get('selesai/{id}', 'PesertaController@selesai')->name('peserta.selesai');
            route::get('status', 'PesertaController@status')->name('peserta.status');
        });
        route::get('alumni', 'PesertaController@alumni')->name('alumni');
        Route::group(['prefix' => 'print'], function () {
            route::get('pendaftaran', 'PrintController@pendaftaran')->name('print.pendaftaran');
            route::get('selesai/{id}', 'PrintController@selesai')->name('print.selesai');
            route::get('downloadfilepeserta/{id}', 'PrintController@downloadfilepeserta')->name('print.downloadfilepeserta');
            route::get('downloadfileadmin/{id}', 'PrintController@downloadfileadmin')->name('print.downloadfileadmin');
        });
        Route::group(['prefix' => 'informasi'], function () {
            route::get('/', 'InformasiController@index')->name('informasi');
            route::get('create', 'InformasiController@create')->name('informasi.create');
            route::post('store', 'InformasiController@store')->name('informasi.store');
            route::get('edit', 'InformasiController@edit')->name('informasi.edit');
            route::post('update', 'InformasiController@update')->name('informasi.update');
            route::get('delete/{id}', 'InformasiController@delete')->name('informasi.delete');
            route::get('datatable', 'InformasiController@datatable')->name('informasi.datatable');
        });
        Route::group(['prefix' => 'setting'], function () {
            route::get('basic', 'SettingController@basic')->name('setting.basic');
            route::post('basic-update', 'SettingController@basicupdate')->name('setting.basic-update');
            route::get('basic-edit', 'SettingController@basic-edit')->name('setting.basic-edit');
            route::get('header', 'SettingController@header')->name('setting.header');
            route::post('header-update', 'SettingController@headerupdate')->name('setting.header-update');
            route::get('body', 'SettingController@body')->name('setting.body');
            route::post('body-update', 'SettingController@bodyupdate')->name('setting.body-update');
            route::get('footer', 'SettingController@footer')->name('setting.footer');
            route::post('footer-update', 'SettingController@footerupdate')->name('setting.footer-update');
        });
    });
});
