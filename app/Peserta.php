<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $table = 'peserta';
    protected $fillable = [
        'id',
        'nama',
        'institusi',
        'program_studi',
        'jenis_kelamin',
        'no_telepon',
        'pembina_id',
        'user_id',
        'bagian',
        'status',
        'created_at',
        'updated_at'
    ];
    public function pembina(){
        return $this->belongsTo('App\user','pembina_id');
    }
    public function user(){
        return $this->belongsTo('App\user','user_id');
    }
}
