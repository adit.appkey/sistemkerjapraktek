<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frontend extends Model
{
    protected $table='frontend';
    protected $fillable = [
        'title', 
        'body', 
        'footer',
        'created_at',
        'updated_at',
    ];
}
