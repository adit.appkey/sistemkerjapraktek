<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table='gallery';
    protected $fillable = [
        'id', 
        'title', 
        'image',
        'created_at',
        'updated_at',
    ];
}
