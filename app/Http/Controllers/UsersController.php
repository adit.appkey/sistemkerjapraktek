<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Role;
use App\Peserta;
use DataTables;
use Validator;
use Session;
use Hash;
use Carbon\Carbon;

class UsersController extends Controller
{
    public function index()
    {
        $user = User::orderBy('created_at', 'DESC')->get();
        $roles = Role::all();
        // $stores = Store::all();
        return view('user.index', compact('user','roles'));
    }
    public function datatable(Request $request)
    {
        if($request->id_store){
            $data= User::where('role_id', 'like', '%' . $request->id_role . '%')->where('store_id', 'like', '%' . $request->id_store . '%')->orderby('id', 'DESC')->where('deleted_at', null)->get();
        }else{
            $data= User::where('role_id', 'like', '%' . $request->id_role . '%')->orderby('id', 'DESC')->where('deleted_at', null)->get();
        }
     
        return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('action', function($data) {
                return  
                '<a href="'.url('user/edit/'.$data->id, []).'" class="btn btn-edit"><i class="far fa-edit"></i></a>'. 
                '<a href="#"onclick="deleteConfirm('.$data->id.')" class="btn btn-del"><i class="fa fa-trash"></i></a>';
                // '<button class="btn btn-edit" onclick="window.location.href="'.url('user/edit/'.$data->id, []).'""><i class="far fa-edit"></i></button>'.
                // '<button class="btn btn-del" onclick="deleteConfirm('.$data->id.')"><i class="fa fa-trash"></i></button>';
            })
            // ->editColumn('image', function($data){
            //     return 
            //     '<img src="'.asset('image/product/'.$data->image).'" alt="" height="100">';
            // })
            ->editColumn('name', function($data){
                if($data->image != null)
                {
                    $image = '<img src="'.asset('images/'.$data->image).'" alt="Avatar Tailwind CSS Component">';
                }
                else
                {
                    $image = '<img src="'.asset('image/default-user.png').'" alt="Avatar Tailwind CSS Component">';
                }
                return 
               '<div class="flex items-center space-x-3">'.
                              '<div class="avatar">'.
                                '<div class="w-12 h-12 mask mask-squircle">'.
                                    $image.
                                '</div>'.
                              '</div> '.
                              '<div>'.
                                '<div class="font-bold">'.
                                    $data->name.
                                '</div> '.
                              '</div>'.
                            '</div>';
            })
            ->editColumn('role', function($data){
                return $data->role->name ?? '--';
            })
            ->editColumn('status', function($data){
                if($data->status == 1){
                    return'<button class="btn btn-accent" style="color:white;background-color: #32dbb7"disabled>active</button> ';
                }else{
                    return'<button class="btn btn-accent" style="color:white; background-color: red" disabled >non active</button> ';
                }
            })
            // ->editColumn('checkbox', function($data){
            //     return
            //     $data->id;
            // })
            ->rawColumns(['action','name','store','role','status','idi'])
            ->make(true);
    }
    public function create()
    {
        $roles = Role::all();
        // $stores = Store::all();
        return view('user.create', compact('roles'));
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
            'role' => 'required',
        ]);
        // if(! $request->image){
        //     Session::flash('message', 'image field is required'); Session::flash('alert-class', 'alert-danger');
        //     return redirect()->back()
        //     ->withErrors($validator)
        //     ->withInput();
        // }
        if ($validator->fails()) {
            return redirect()->route('user.create')
                        ->withErrors($validator)
                        ->withInput();
        }
        $item=new User();
        $item->name=$request->name;
        $item->email=$request->email;

        if (User::where('email', $request->email)->exists()) 
        {
        Session::flash('message',\Lang::get('val.emailexist')); Session::flash('alert-class', 'alert-danger'); return back()->withInput($request->all());
        }

        // $item->status=$request->status;
        $item['password'] = Hash::make($request->password);

        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }

        $item->role_id = $request->role;
        $item->store_id = $request->store;
        $item->idi=$request->idi;
        $item->status=1;

        $item->nip=$request->nip;
        $item->jabatan=$request->jabatan;
        $item->pangkat=$request->pangkat;

        $password = $request->password;
        $password_confirmation = $request->password_confirmation;
        if ($password != $password_confirmation)
        {
            Session::flash('message', \Lang::get('val.wrongpass')); Session::flash('alert-class', 'alert-danger'); return back()->withInput($request->all()); 
        }
        $item->save();
        return redirect()->route('user')->with('success' ,'success create user');
    }
    public function edit($id)
    {
        $items = User::find($id);
        // $stores = Store::all();
        $user = User::find($id);
        return view('user.edit', compact('items','user'));
    }
    public function update(Request $request, $id)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id
            // 'password' => 'required|min:8',
            // 'password_confirmation' => 'required|same:password',
            // 'role' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->route('user.edit', ['id'=> $id])
                        ->withErrors($validator)
                        ->withInput();
        }
      
            $item=User::find($id);
            $item->name=$request->name;
            $item->email=$request->email;

            // if (User::where('email', $request->email)->exists()) 
            // {
            // Session::flash('message',\Lang::get('val.emailexist')); Session::flash('alert-class', 'alert-danger'); return back()->withInput($request->all());
            // }

            $item->status=$request->status;
            if($request->password){
                    $validator = Validator::make($request->all(), [
                        'password' => 'required|min:8',
                        'password_confirmation' => 'required|same:password',
                    ]);
                    
                    if ($validator->fails()) {
                        return redirect()->route('user.edit', ['id'=> $id])
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                $password = $request->password;
                $password_confirmation = $request->password_confirmation;
                if ($password != $password_confirmation)
                {
                    Session::flash('message', \Lang::get('val.wrongpass')); Session::flash('alert-class', 'alert-danger'); return back()->withInput($request->all()); 
                }
                $item['password'] = Hash::make($request->password);
            }

        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        $item->nip=$request->nip;
        $item->jabatan=$request->jabatan;
        $item->pangkat=$request->pangkat;
        $item->save();
        return redirect()->route('user')->with('success' ,'success edit user');
    }
    public function delete($id) {
        $item=User::find($id);
        if($item->role_id == 3){
            $peserta = Peserta::find($item->peserta_id);
            if($peserta != null){
                $peserta->deleted_at = Carbon::now()->toDateTimeString();
                $peserta->save();
            }
        }
        $item->deleted_at = Carbon::now()->toDateTimeString();
        $item->save();
        return redirect()->route('user')->with('success' ,'success to delete user');
    }
    public function deleteall(Request $request) {
        $ids= $request->input('ids');
        if( $ids == null){
            return redirect()->route('user')->with('error' ,'no data selected');
        }
        User::whereIn('id',$ids)->delete();
        return redirect()->route('user')->with('success' ,'success to delete user');
    }
    public function upload() {
        // return "hello";
     // Allowed origins to upload images
    // $accepted_origins = array("http://localhost", "http://107.161.82.130", "http://codexworld.com");
    $accepted_origins = array("http://localhost");

    // Images upload path
    $imageFolder = "images/";

    reset($_FILES);
    $temp = current($_FILES);
    if(is_uploaded_file($temp['tmp_name'])){
        if(isset($_SERVER['HTTP_ORIGIN'])){
            // Same-origin requests won't set an origin. If the origin is set, it must be valid.
            if(in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)){
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            }else{
                header("HTTP/1.1 403 Origin Denied");
                return;
            }
        }
    
        // Sanitize input
        if(preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])){
            header("HTTP/1.1 400 Invalid file name.");
            return;
        }
    
        // Verify extension
        if(!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))){
            header("HTTP/1.1 400 Invalid extension.");
            return;
        }
    
            // Accept upload if there was no origin, or if it is an accepted origin
            $filetowrite = $imageFolder . $temp['name'];
            move_uploaded_file($temp['tmp_name'], $filetowrite);
        
            // Respond to the successful upload with JSON.
            echo json_encode(array('location' => $filetowrite));
        } else {
            // Notify editor that the upload failed
            header("HTTP/1.1 500 Server Error");
        }
    }
}
