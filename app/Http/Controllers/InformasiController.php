<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Informasi;

class InformasiController extends Controller
{
    public function index()
    {
        $item = Informasi::where('id', 1)->first();
        return view('informasi.index', compact('item'));
    }
    public function edit()
    {
        $item = Informasi::where('id', 1)->first();
        return view('informasi.edit', compact('item'));
    }
    public function update(Request $request)
    {
        $item = Informasi::where('id', 1)->first();
        $item->rincian = $request->rincian;
        $item->save();
       return redirect()->route('informasi');
    }
}
