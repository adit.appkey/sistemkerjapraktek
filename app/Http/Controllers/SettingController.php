<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Company;
use App\Frontend;
use App\Gallery;
use Validator;

class SettingController extends Controller
{
    public function basic()
    {
        $item = Company::where('id', 1)->first();
        return view('setting.basic',compact('item'));
    }
    public function basicupdate(request $request)
    {
        $item = Company::where('id', 1)->first();
        $item->name = $request->name;
        $item->telepon = $request->telepon;
        $item->alamat = $request->alamat;
        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        $item->save();
        return redirect()->route('dashboard')->with('success' ,'berhasil diperbarui');
    }
    public function header()
    {
        $item = Frontend::where('id', 1)->first();
        return view('setting.header',compact('item'));
    }
    public function headerupdate(request $request)
    {
        $item = Frontend::where('id', 1)->first();
        $item->header = $request->header;
        $item->save();
        return redirect()->route('dashboard')->with('success' ,'berhasil diperbarui');
    }
    public function body()
    {
        $item = Frontend::where('id', 1)->first();
        return view('setting.body',compact('item'));
    }
    public function bodyupdate(request $request)
    {
        $item = Frontend::where('id', 1)->first();
        $item->body = $request->body;
        $item->save();
        return redirect()->route('dashboard')->with('success' ,'berhasil diperbarui');
    }
    public function footer()
    {
        $item = Frontend::where('id', 1)->first();
        return view('setting.footer',compact('item'));
    }
    public function footerupdate(request $request)
    {
        $item = Frontend::where('id', 1)->first();
        $item->footer = $request->footer;
        $item->save();
        return redirect()->route('dashboard')->with('success' ,'berhasil diperbarui');
    }
    public function galleryindex(request $request)
    {
        $item = Gallery::all();
        return view('setting.galleryindex',compact('item'));
    }
    public function gallerycreate()
    {
        return view('setting.gallerycreate');
    }
    public function gallerystore(request $request)
    {
        $item = new Gallery();
        $item->title = $request->title;
        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        $item->save();
        return redirect()->route('setting.galleryindex')->with('success' ,'berhasil menambah gallery');
    }
    public function galleryedit(request $request, $id)
    {
        $item = Gallery::find($id);
        return view('setting.gallerycreate', compact('item'));
    }
    public function galleryupdate(request $request, $id)
    {
        $item = Gallery::find($id);
        $item->title = $request->title;
        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        $item->save();
        return redirect()->route('setting.galleryindex')->with('success' ,'berhasil mengubah gallery');
    }
    public function gallerydelete($id)
    {
        $item = Gallery::find($id);
        $item->delete();
        return redirect()->route('setting.galleryindex')->with('success' ,'berhasil menghapus gallery');
    }
}
