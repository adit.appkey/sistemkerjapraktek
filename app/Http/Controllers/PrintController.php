<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peserta;
use App\User;
use PDF;

use Illuminate\Support\Facades\Storage;
use Response;

class PrintController extends Controller
{
    public function pendaftaran()
    {
        $id = auth()->user()->id;
        $item = Peserta::where('user_id', $id)->first();
        $pembina = User::where('id', $item->pembina_id)->first();
        $peserta = Peserta::find($item->user_id);
        if($item->status < 3){
            return redirect()->back()->with('error' ,'pembina belum menerima permintaan anda! mohon menunggu status hingga diterima!');
        }
        if($item->pembina_id == null){
            return redirect()->back()->with('error' ,'pembina belum menerima permintaan anda! mohon menunggu status hingga diterima!');
        }
        $pdf = PDF::loadView("print.pendaftaran", compact('item','pembina'))->setPaper('F4','potrait');
        return $pdf->stream();
        // return view('print.pendaftaran', compact('item','pembina'));
    }
    public function selesai($id)
    {
        $item = Peserta::find($id);
        $pembina = User::where('id', $item->pembina_id)->first();
        if($item->pembina_id == null){
            return redirect()->back()->with('error' ,'pembina belum menerima permintaan anda! mohon menunggu status hingga diterima!');;
        }
        $pdf = PDF::loadView("print.selesai", compact('item','pembina'))->setPaper('F4','potrait');
        return $pdf->stream();
        // return view('print.selesai', compact('item','pembina'));
    }
    public function downloadfilepeserta($id)
    {
        $item = Peserta::where('id',  $id)->first();
        if($item->file_peserta == null){
            return redirect()->back()->with('error' ,'file kosong!');
        }
        $file = Storage::disk('public')->path("filepeserta/$item->file_peserta");
        $headers = array(
              'Content-Type: application/pdf',
            );
        return Response::download($file, $item->file_peserta, $headers);
    }
    public function downloadfileadmin($id)
    {
        $item = Peserta::where('user_id',  $id)->first();
        if($item->file_admin == null){
            return redirect()->back()->with('error' ,'file kosong!');
        }
        $file = Storage::disk('public')->path("fileadmin/$item->file_admin");
        $headers = array(
              'Content-Type: application/pdf',
            );
        return Response::download($file, $item->file_admin, $headers); 
    }
}
