<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class FrontController extends Controller
{
    public function index()
    {
        $item = Company::where('id', 1)->first();
        return view('frontend.index', compact('item'));
    }
}
