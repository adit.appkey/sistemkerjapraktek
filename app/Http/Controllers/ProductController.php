<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Validator;
use Session;
use App\Category;
use App\Product;
use App\Store;

class ProductController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        if($user->role_id == 1){
            $items = Product::whereNull('deleted_at')->with('category')->get();
            return view('product.index',compact('items'));
        }else{
            $items = Product::where('store_id', $user->store_id)->whereNull('deleted_at')->with('category')->get();
            return view('product.index',compact('items')); 
        }
    }
    public function create()
    {
        $categories = Category::whereNull('deleted_at')->get();
        $stores = Store::all();
        return view('product.create', compact('categories','stores'));
    }
    public function store(Request $request)
    { 
        $user = auth()->user();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('product.create')
                        ->withErrors($validator)
                        ->withInput();
        }
        $item = new Product;
        $item->name = $request->name;
        $item->category_id = $request->category_id;
        $item->price = $request->price;
        $item->stock = $request->stock;
        $item->description = $request->description;
        $item->status = $request->status;
        if($user->role_id == 2){
            $item->store_id = $user->store_id;
        }else{
            $item->store_id = $request->store_id;
        }
        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        $item->save();
        return redirect()->route('product')->with('success' ,'success create product');
    }
    public function show($id)
    {
        $items = Product::find($id);
        return view('product.show');
    }
    public function edit($id)
    {
        $categories = Category::whereNull('deleted_at')->get();
        $stores = Store::all();
        $item = Product::find($id);
        return view('product.edit', compact('item','categories','stores'));
    }
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('product.edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        $item = Product::find($id);
        $item->name = $request->name;
        $item->category_id = $request->category_id;
        $item->store_id = $request->store_id;
        $item->price = $request->price;
        $item->stock = $request->stock;
        $item->description = $request->description;
        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        // if(sizeof($images) != 0){
        //     $item->image = implode('|',$images);
        // }
        $item->description = $request->description;
        $item->status = $request->status;
        // return $item;
        $item->save();
        return redirect()->route('product')->with('success' ,'success update product');
    }
    public function delete($id)
    {
        $item = Product::find($id);
        $item->deleted_at = Carbon::now()->toDateTimeString();
        $item->save();
        return redirect()->back();
    }
}
