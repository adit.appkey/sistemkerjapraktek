<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Role;
use App\Peserta;
use App\Informasi;
use Validator;

class DashboardController extends Controller
{
    public function index()
    {
        $admin = User::where('role_id', 1)->where('deleted_at', null)->get();
        $admin = count($admin);

        $pembina = User::where('role_id', 2)->where('deleted_at', null)->get();
        $pembina = count($pembina);

        $peserta = peserta::where('status', 3)->where('deleted_at', null)->get();
        $peserta = count($peserta);

        $alumni = peserta::where('status', 5)->where('deleted_at', null)->get();
        $alumni = count($alumni);
        $informasi = Informasi::where('id', 1)->first();
        return view('dashboard.index', compact('admin','pembina','peserta','alumni','informasi'));
    }
    public function profile()
    {
        $items = auth()->user();
        $user = User::find($items->id);
        return view('dashboard.profile', compact('items'));
    }
    public function update(request $request)
    {
         $id = $request->id;
         $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id
            // 'password' => 'required|min:8',
            // 'password_confirmation' => 'required|same:password',
            // 'role' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->route('user.edit', ['id'=> $id])
                        ->withErrors($validator)
                        ->withInput();
        }
      
            $item=User::find($id);
            $item->name=$request->name;
            $item->email=$request->email;

            // if (User::where('email', $request->email)->exists()) 
            // {
            // Session::flash('message',\Lang::get('val.emailexist')); Session::flash('alert-class', 'alert-danger'); return back()->withInput($request->all());
            // }
            if($request->password){
                    $validator = Validator::make($request->all(), [
                        'password' => 'required|min:8',
                        'password_confirmation' => 'required|same:password',
                    ]);
                    
                    if ($validator->fails()) {
                        return redirect()->route('user.edit', ['id'=> $id])
                                    ->withErrors($validator)
                                    ->withInput();
                    }
                $password = $request->password;
                $password_confirmation = $request->password_confirmation;
                if ($password != $password_confirmation)
                {
                    Session::flash('message', \Lang::get('val.wrongpass')); Session::flash('alert-class', 'alert-danger'); return back()->withInput($request->all()); 
                }
                $item['password'] = Hash::make($request->password);
            }

        if($request->image){
            $file = $request->image;
            $file_name = time().'-'.Str::random(50).'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/'), $file_name);
            $item->image = $file_name;
        }
        $item->nip=$request->nip;
        $item->jabatan=$request->jabatan;
        $item->pangkat=$request->pangkat;
        $item->save();
        return redirect()->route('dashboard')->with('success' ,'success edit user');
    }
}
