<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Peserta;
use DataTables;
use Validator;
use Session;
use Hash;
use Carbon\Carbon;

class PesertaController extends Controller
{
    public function daftar()
    {
        $items = Peserta::where('status', 1)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.index', compact('items'));
    }
    public function pending()
    {
        if(auth()->user()->role_id == 2){
            $items = Peserta::where('status', 2)->where('pembina_id', auth()->user()->id)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.index', compact('items'));
        }
        $items = Peserta::where('status', 2)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.index', compact('items'));
    }
    public function verified()
    {
        if(auth()->user()->role_id == 3){
            $items = Peserta::where('status', 2)->where('pembina_id', auth()->user()->id)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.index', compact('items'));
        }
        $items = Peserta::where('status', 3)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.index', compact('items'));
    }
    public function rejected()
    {
        $items = Peserta::where('status', 4)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.index', compact('items'));
    }
    public function alumni()
    {
        $items = Peserta::where('status', 5)->orderBy('created_at', 'DESC')->where('deleted_at', null)->get();
        return view('peserta.alumni', compact('items'));
    }
    public function terima($id)
    {
        $item = Peserta::find($id);
        if($item->status == 1){
            $pembina = User::where('role_id', 2)->where('deleted_at', null)->get();
            return view('peserta.choose', compact('item','pembina'));
        }else{
            $item->status = 3;
            $item->save();
            return redirect()->route('peserta.verified')->with('success' ,'peserta berhasil diterima kerja praktek');
        }
    }
    public function terimastore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pembina_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $item = Peserta::find($request->id);
        $item->status = 2;
        $item->pembina_id = $request->pembina_id;
        if($request->file_admin){
            $file = $request->file_admin;
            $file_name = time().'-'.Str::random(8, 'abcdefghijklmnopqrstuvwxyz').'.'.$file->getClientOriginalExtension();
            $file->move(storage_path('app/public/fileadmin/'), $file_name);
            $item->file_admin = $file_name;
        }
        $item->no_surat = $request->no_surat;
        $item->tanggal_surat = $request->tanggal_surat;
        $item->kepada = $request->kepada;
        $item->save();
        return redirect()->route('peserta.pending')->with('success' ,'verifikasi berhasil, selanjutnya peserta akan menunggu persetujuan dari pembina');
    }
    public function tolak($id)
    {
        $item = Peserta::find($id);
        $item->status = 4;
        $item->save();
        return redirect()->route('peserta.rejected')->with('success' ,'peserta ditolak!');
    }
    public function selesai($id)
    {
        $user_id = auth()->user()->role_id;
        $item = Peserta::find($id);
        $item->status = 5;
        $item->save();
        if($user_id == 1){
            return redirect()->route('alumni')->with('success' ,'peserta telah selesai kerja praktek! silahkan download surat selesai kerja praktek!');
        }else{
            return redirect()->route('peserta.verified')->with('success' ,'peserta telah selesai kerja praktek! silahkan download surat selesai kerja praktek!');
        }
        
    }
    public function edit($id)
    {
        $item = Peserta::find($id);
        $pembina = User::where('role_id', 2)->where('deleted_at', null)->get();
        return view('peserta.edit', compact('item','pembina'));
    }
    public function update(Request $request, $id)
    {
        $item = Peserta::find($id);
        $item->name = $request->name;
        $item->no = $request->no;
        $item->institusi = $request->institusi;
        $item->program_studi = $request->program_studi;
        $item->jenis_kelamin = $request->jenis_kelamin;
        $item->no_telepon = $request->no_telepon;
        $item->pembina_id = $request->pembina_id;
        $item->bagian = $request->bagian;
        $item->start = $request->start;
        $item->end = $request->end;
        $item->no_surat = $request->no_surat;
        $item->tanggal_surat = $request->tanggal_surat;
        $item->kepada = $request->kepada;
        if($request->file_admin){
            $file = $request->file_admin;
            $file_name = time().'-'.Str::random(8, 'abcdefghijklmnopqrstuvwxyz').'.'.$file->getClientOriginalExtension();
            $file->move(storage_path('app/public/fileadmin/'), $file_name);
            $item->file_admin = $file_name;
        }
        $item->save();
        return redirect()->back()->with('success' ,'Berhasil Update Data Peserta');
    }
    public function create()
    {
        $pembina = User::where('role_id', 2)->where('deleted_at', null)->get();
        return view('peserta.create', compact('pembina'));
    }
    public function store(Request $request)
    {
        $id = auth()->user()->id;
        $item = new Peserta();
        $item->name = $request->name;
        $item->no = $request->no;
        $item->institusi = $request->institusi;
        $item->program_studi = $request->program_studi;
        $item->jenis_kelamin = $request->jenis_kelamin;
        $item->no_telepon = $request->no_telepon;
        $item->pembina_id = $request->pembina_id;
        $item->bagian = $request->bagian;
        $item->status = 1;
        $item->start = $request->start;
        $item->end = $request->end;
        $item->user_id = $id;
        if($request->file_peserta){
            $file = $request->file_peserta;
            $file_name = time().'-'.Str::random(8, 'abcdefghijklmnopqrstuvwxyz').'.'.$file->getClientOriginalExtension();
            $file->move(storage_path('app/public/filepeserta/'), $file_name);
            $item->file_peserta = $file_name;
        }
        $item->save();
        $user = User::find($id);
        $user->peserta_id = $item->id;
        $user->save();
        return redirect()->back()->with('success' ,'Berhasil mendaftarkan sebagai peserta KP');
    }
    public function status()
    {
        $id = auth()->user()->id;
        $item = Peserta::where('user_id', $id)->first();
        if($item == null){
            return redirect()->back()->with('error' ,'Anda belum mendaftar! silahkan daftar terlebih dahulu!');
        }else{
            if($item->status == 1){
                $status = 'Menunggu Verifikasi';
            }elseif($item->status == 2){
                $status = 'Menunggu Persetujuan Pembina';
            }elseif($item->status == 3){
                $status = 'Diterima !';
            }elseif($item->status == 4){
                $status = 'Ditolak !';
            }elseif($item->status == 5){
                $status = 'Lulus';
            }
            return view('peserta.status', compact('item','status'));   
        }
    }
}
