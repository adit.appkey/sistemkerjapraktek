<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRoleAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if(auth()->user()->status == 1){
            return $next($request);
        }else{
            Auth::logout();
            return redirect('/login');
        }
    }
}
